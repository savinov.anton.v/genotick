package com.alphatica.genotick.breeder;

import com.alphatica.genotick.genotick.Engine;
import com.alphatica.genotick.mutator.Mutator;

public class RobotBreederFactory {
    public static RobotBreeder getDefaultBreeder(BreederSettings breederSettings, Mutator mutator, Engine engine) {
        RobotBreeder breeder = SimpleBreeder.getInstance();
        breeder.setSettings(breederSettings, mutator, engine);
        return breeder;
    }
}
