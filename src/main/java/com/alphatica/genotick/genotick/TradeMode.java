package com.alphatica.genotick.genotick;

public enum TradeMode {
    AT_NEXT_OPEN("open", 1),
    AT_NEXT_CLOSE("close", 4);

    private String mode;
    private int column;

    TradeMode(String mode, int column) {
        this.mode = mode;
        this.column = column;
    }

    public String getMode() {
        return mode;
    }

    public int getColumn() {
        return column;
    }

    public static TradeMode fromMode(String mode) {
        for(TradeMode tradeMode: values()) {
            if (mode.equals(tradeMode.mode)) {
                return tradeMode;
            }
        }
        throw new IllegalArgumentException("No such mode: " + mode);
    }
}
