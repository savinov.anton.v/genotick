package com.alphatica.genotick.genotick;

public class EngineResult {

    private final long executionTime;

    public EngineResult(long executionTime) {
        this.executionTime = executionTime;
    }

    public long getExecutionTime() {
        return executionTime;
    }
}
